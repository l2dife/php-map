<?php
  require '../src/L2Dife/Map/Map.php';
  require '../src/L2Dife/Map/Point.php';

  use L2Dife\Map\Map;

  $map = new Map(906, 1310);

  $queen = $map->calculate(-21637, 181767);
  $baium = $map->calculate(115213, 16623);
  $valakas = $map->calculate(213004, -114890);
  $antharas = $map->calculate(185708, 114298);

  ?><!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Van Neves">
    <title>L2 Map</title>
    <style type="text/css">
      body { margin: 0; padding: 0; }
      .map { width: <?= $map->getWidth() ?>px; height: <?= $map->getHeight() ?>px; position: relative; margin: auto; }
      .point { position: absolute; font-size: 16px; font-weight: bold; color: red; }
    </style>
  </head>
  <body>
    <div class="map">
      <span class="point" style="left: <?= $queen->x ?>px; top: <?= $queen->y ?>px;">+</span>
      <span class="point" style="left: <?= $baium->x ?>px; top: <?= $baium->y ?>px;">+</span>
      <span class="point" style="left: <?= $valakas->x ?>px; top: <?= $valakas->y ?>px;">+</span>
      <span class="point" style="left: <?= $antharas->x ?>px; top: <?= $antharas->y ?>px;">+</span>

      <img src="img/map.small.jpg" alt="Lineage 2 Interlude Map">
    </div>
  </body>
</html>