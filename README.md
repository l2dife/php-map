![Logo](https://bitbucket.org/repo/64dLBKp/images/2994267402-l2dife-logo.png)

# L2 Interlude Map

Biblioteca PHP para converter a posição de um jogador ou NPC (posições X e Y) em píxels no mapa.

## Instalação

```bash
composer require l2dife/php-map
```

## Exemplo

Para utilizar é muito simples, basta importar a biblioteca (fazer o include se não tiver autoload), criar uma instância de `Map` passando como parâmetro a largura e a altura da sua imagem. Depois você chama o método `calculate()` passando a posição X e Y desejada (no boss, mob, npc ou player). O método irá retornar uma instância de `Point` que contém as propriedades `x` e `y`, que são as posições em píxels na imagem.

```php
use L2Dife\Map\Map;

// tamanho da imagem mapa (largura e altura)
$map = new Map(906, 1310);

// coordenadas X e Y que deseja
$queen_ant = $map->calculate(-21637, 181767);

echo $queen_ant->x; // posição X na imagem
echo $queen_ant->y; // posição Y na imagem

```

Fiz e testei no mapa do Interlude, mas funciona no C4, C5 e CT1.

## Inspiração

Quando comecei a jogar L2, em 2007, era em um servidor privado Interlude de 5x. No site do servidor dava para buscar pelo charname e ver onde o jogador estava. Muito bom para fazer um PvP com os amigos.

Outro lugar, que muito usei na época, foi o [pmfun](http://lineage.pmfun.com/loc/21362/varka-silenos-officer.html) para ver onde drop de itens.