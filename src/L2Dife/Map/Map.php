<?php

namespace L2Dife\Map;

class Map
{
    const BASE_WIDTH = 1812;

    const BASE_HEIGHT = 2620;

    const BASE_ADJUSTMENT_X = 655;

    const BASE_ADJUSTMENT_Y = 1310;

    const BASE_SCALE = 199.55;

    private $width;

    private $height;

    private $scale_x;

    private $scale_y;

    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;

        $this->scale_x = self::BASE_WIDTH / $this->width;
        $this->scale_y = self::BASE_HEIGHT / $this->height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function calculate($x, $y)
    {
        $x = ($x / self::BASE_SCALE) + self::BASE_ADJUSTMENT_X;
        $y = ($y / self::BASE_SCALE) + self::BASE_ADJUSTMENT_Y;

        $x = $x / $this->scale_x;
        $y = $y / $this->scale_y;

        return new Point($x, $y);
    }
}