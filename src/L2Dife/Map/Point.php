<?php

namespace L2Dife\Map;

class Point
{
    public $x = 0;

    public $y = 0;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
}